# st-patches

This project aims to ease patching suckless st.

`patches.orig` contains a list with original patches. Run the command below to fetch them

```
make fetch
```
